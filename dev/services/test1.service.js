import FakeTransaction from '~/dev/fakeTransaction';

let object = {
  master: 'cheburek',
  a: 'a',
  'a.1': 'a.1',
  'a.2': 'a.2',
  b: 'b',
  'b.1': 'b.1',
  'b.2': 'b.2',
  'b.3': 'b.3',
};

let errorProp = null;

export default {
  name: 't',
  actions: {
    start: {
      transaction: true,
      // compensator(ctx) {
      //   return revertKek();
      // },
      async handler(ctx) {
        const transaction = ctx.locals.$transaction = new FakeTransaction(object, 'master', ctx);

        transaction.update('updated.from.master');

        await Promise.all([
          ctx.call('t.a'),
          ctx.call('t.b'),
        ]);

        return {
          kek: true,
        };
      },
    },
    a: {
      transaction: true,
      async handler(ctx) {
        const transaction = ctx.locals.$transaction = new FakeTransaction(object, ctx.params.prop || 'a', ctx);

        const timeout = Math.floor(Math.random() * 2000);

        this.logger.info(`Handling ${ctx.params.prop || 'a'} within ${timeout}ms`);

        await new Promise(resolve => setTimeout(resolve, timeout));

        transaction.update(`updated.from.${ctx.params.prop || 'a'}`);

        if (!ctx.params.prop) {
          await Promise.all([
            ctx.call('t.a', {
              prop: 'a.1',
            }),
            ctx.call('t.a', {
              prop: 'a.2',
            }),
          ]);
        } else if (ctx.params.prop === errorProp) {
          throw new Error('ERROR');
        }
      },
    },
    b: {
      transaction: true,
      async handler(ctx) {
        const transaction = ctx.locals.$transaction = new FakeTransaction(object, ctx.params.prop || 'b', ctx);

        const timeout = Math.floor(Math.random() * 2000);

        this.logger.info(`Handling ${ctx.params.prop || 'b'} within ${timeout}ms`);

        await new Promise(resolve => setTimeout(resolve, timeout));

        transaction.update(`updated.from.${ctx.params.prop || 'b'}`);

        if (!ctx.params.prop) {
          await Promise.all([
            ctx.call('t.b', {
              prop: 'b.1',
            }),
            ctx.call('t.b', {
              prop: 'b.2',
            }),
            ctx.call('t.b', {
              prop: 'b.3',
            }),
          ]);
        } else if (ctx.params.prop === errorProp) {
          throw new Error('ERROR');
        }
      },
    },
    async test(ctx) {
      if (ctx.params.errorProp) {
        errorProp = ctx.params.errorProp;
      }
      return await ctx.call('t.start')
        .then(() => {
          return true;
        })
        .catch(() => {
          return false;
        });
    },
    async print(ctx) {
      this.logger.info('current object is', object);
    },
    async reset(ctx) {
      object = {
        master: 'cheburek',
        a: 'a',
        'a.1': 'a.1',
        'a.2': 'a.2',
        b: 'b',
        'b.1': 'b.1',
        'b.2': 'b.2',
        'b.3': 'b.3',
      };
      return object;
    },
  },
  methods: {},
  started() {},
};
