/* eslint-disable */
/*
 * moleculer-bull
 * Copyright (c) 2017 MoleculerJS (https://github.com/moleculerjs/moleculer-addons)
 * MIT Licensed
 */

'use strict';

let _ = require('lodash');
let Queue = require('bull');

module.exports = function createService(url, queueOpts) {

  /**
   * Task queue mixin service for Bull
   *
   * @name moleculer-bull
   * @module Service
   */
  return {
    name: 'bull',
    methods: {
      /**
       * Create a new job
       *
       * @param {String} name
       * @param {any} jobName
       * @param {any} payload
       * @param {any} opts
       * @returns {Job}
       */
      createJob(name, jobName, payload, opts) {
        if (opts) {
          return this.getQueue(name)
            .add(jobName, payload, opts);
        } else if (payload) {
          return this.getQueue(name)
            .add(jobName, payload);
        } else {
          return this.getQueue(name)
            .add(jobName);
        }
      },

      /**
       * Get a queue by name
       *
       * @param {String} name
       * @returns {Queue}
       */
      getQueue(name) {
        if (!this.$queues[name]) {
          this.$queues[name] = Queue(name, url, queueOpts);
        }
        return this.$queues[name];
      },
    },
    started() {
      this.$queues = {};

      if (this.schema.queues) {
        _.forIn(this.schema.queues, (fn, name) => {
          const queue = this.getQueue(name);

          if (typeof fn === 'function') {
            queue
              .process('*', fn.bind(this));
          } else if (typeof fn === 'object') {
            if (Array.isArray(fn)) {
              fn.forEach((elem) => {
                let args = [];

                if (elem.name) {
                  args.push(elem.name);
                } else {
                  args.push('*');
                }

                if (elem.concurrency) {
                  args.push(elem.concurrency);
                }

                args.push(elem.process.bind(this));

                queue.process(...args);

                if (elem.on) {
                  Object
                    .entries(elem.on)
                    .forEach(([event, handler]) => {
                      queue.on(event, handler.bind(this));
                    });
                }
              });
            } else {
              let args = [];

              if (fn.name) {
                args.push(fn.name);
              } else {
                args.push('*');
              }

              if (fn.concurrency) {
                args.push(fn.concurrency);
              }

              args.push(fn.process.bind(this));

              queue.process(...args);

              if (fn.on) {
                Object
                  .entries(fn.on)
                  .forEach(([event, handler]) => {
                    queue.on(event, handler.bind(this));
                  });
              }
            }
          }
        });
      }

      return this.Promise.resolve();
    },
    stopped() {
      Object
        .entries(this.$queues)
        .forEach(([name, queue]) => {
          queue.close();
          delete this.$queues[name];
        });
    },
  };
};