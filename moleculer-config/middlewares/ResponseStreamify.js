import http from 'http';
import { Readable } from 'stream';
import streamToBuffer from '../../util/streamBuffer/streamToBuffer';

export default function (broker) {
  return {
    localAction(handler) {
      return ctx => {
        // Skip ResponseStreamify if action was called by a local node
        if (ctx.nodeID === broker.nodeID) return handler(ctx);

        return handler(ctx)
          .then(response => {
            // Return response if it is undefined, null, false or empty string
            if (!response) return response;

            const isStream = response && response.readable === true && typeof response.on === 'function' && typeof response.pipe === 'function';

            if (isStream) return response;

            ctx.meta.$Streamify_response = true;

            const stream = new Readable({
              objectMode: true,
              read() {},
            });

            stream.push(response);
            stream.push(null);

            return stream;
          });
      };
    },
    remoteAction(handler, action) {
      return ctx => {
        return handler(ctx)
          .then(async (response) => {
            if (!ctx.meta.$Streamify_response) return response;

            const span = ctx.startSpan(`action '${action.name}' ResponseRestify`);

            try {

              return await new Promise((resolve, reject) => {
                response.on('data', resolve);
                response.on('error', reject);
              });
            } catch (err) {
              span.setError(err);
              throw err;
            } finally {
              ctx.finishSpan(span);
            }
          });
      };
    },
  };
};
