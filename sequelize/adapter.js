import Sequelize from 'sequelize';
import fs from 'fs';
import path from 'path';

export default class SequelizeAdapter {
  constructor(opts) {
    this.opts = opts;
    this.db = null;
  }

  init(broker, service) {
    this.broker = broker;
    this.service = service;

    this.db = new Sequelize(
      this.opts.database,
      this.opts.username,
      this.opts.password,
      {
        host: this.opts.host,
        port: this.opts.port,
        dialect: this.opts.dialect || 'postgres',
        operatorsAliases: false,
        logging: false,
        dialectOptions: this.opts.dialectOptions || {
          ssl: Boolean(this.opts.ssl),
        },
        pool: this.opts.pool || {
          max: 30,
          min: 0,
          acquire: 30000,
          idle: 10000,
        },
      },
    );

    if (this.opts.modelsDir) {
      const basename = path.basename(__filename);

      const models = {};

      fs.readdirSync(this.opts.modelsDir)
        .filter(
          (file) =>
            file.indexOf('.') !== 0 &&
            file !== 'index.js' &&
            file.slice(-3) === '.js',
        )
        .forEach((file) => {
          const model = this.db.import(path.join(this.opts.modelsDir, file));
          models[model.name] = model;
        });

      this.models = models;

      Object.values(models).forEach((model) => {
        if (model.associate) {
          model.associate(this);
        }
      });
    }
  }

  async connect() {
    try {
      return this.db.authenticate();
    } catch (error) {
      this.service.logger.error(error);
      throw new Error('Authentication failed!!!');
    }
  }
}
