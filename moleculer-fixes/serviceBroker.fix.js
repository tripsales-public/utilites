// const _ = require('lodash');
// const { ServiceBroker } = require('moleculer');
// const E = require('moleculer/src/errors');
// const { METRIC } = require('moleculer/src/metrics');

// ServiceBroker.prototype.destroyService = function destroyService(service) {
//   if (_.isString(service) || _.isPlainObject(service)) {
//     service = this.getLocalService(service);
//   }
//   if (!service) {
//     return Promise.reject(new E.ServiceNotFoundError({ service }));
//   }
//
//   return Promise.resolve()
//     .then(() => service._stop())
//     .catch(err => {
//       /* istanbul ignore next */
//       this.logger.error(`Unable to stop '${service.name}' service.`, err);
//     })
//     .then(() => {
//       _.remove(this.services, svc => svc == service);
//       this.registry.unregisterService(service.fullName, this.nodeID);
//
//       this.logger.info(`Service '${service.name}' is stopped.`);
//       this.servicesChanged(true);
//
//       this.metrics.set(METRIC.MOLECULER_BROKER_LOCAL_SERVICES_TOTAL, this.services.length);
//
//       return Promise.resolve();
//     });
// };
