import OfficialCacher from 'moleculer/src/cachers/redis';
import _ from 'lodash';

OfficialCacher.prototype._generateKeyFromObject = function _generateKeyFromObject(
  obj,
) {
  if (Array.isArray(obj)) {
    return '[' + obj.map(o => this._generateKeyFromObject(o)).join('|') + ']';
  } else if (_.isObject(obj)) {
    return (
      '{' +
      Object.keys(obj)
        .map(key => [key, this._generateKeyFromObject(obj[key])].join(':'))
        .join('|') +
      '}'
    );
  } else if (obj != null) {
    return obj.toString();
  } else {
    return 'null';
  }
};
