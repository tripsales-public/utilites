/* eslint-disable import/no-extraneous-dependencies */
import _ from 'lodash';
import Promise from 'bluebird';
import OfficialAdapter from 'moleculer-db';

export default {
  mixins: [OfficialAdapter],
  methods: {
    transformSort(paramSort) {
      let sort = paramSort;
      if (_.isString(sort))
        sort = sort.replace(/,/, " ").split(" ");

      if (Array.isArray(sort)) {
        let sortObj = [];
        sort.forEach(s => {
          if (s.startsWith("-"))
            sortObj.push([s.slice(1), "DESC"]);
          else
            sortObj.push([s, "ASC"]);
        });
        return sortObj;
      }

      if (_.isObject(sort)) {
        return Object.keys(sort).map(name => [name, sort[name] > 0 ? "ASC" : "DESC"]);
      }

      /* istanbul ignore next*/
      return [];
    },
    sanitizeParams(ctx, params) {
			let p = Object.assign({}, params);

			// Convert from string to number
			if (typeof(p.limit) === "string")
				p.limit = Number(p.limit);
			if (typeof(p.offset) === "string")
				p.offset = Number(p.offset);
			if (typeof(p.page) === "string")
				p.page = Number(p.page);
			if (typeof(p.pageSize) === "string")
				p.pageSize = Number(p.pageSize);
			// Convert from string to POJO
			if (typeof(p.query) === "string")
        p.query = JSON.parse(p.query);

			if (typeof(p.calculationsQuery) === "string")
				p.calculationsQuery = JSON.parse(p.calculationsQuery);

			if (typeof(p.sort) === "string")
				p.sort = p.sort.replace(/,/g, " ").split(" ");

			if (typeof(p.fields) === "string")
				p.fields = p.fields.replace(/,/g, " ").split(" ");

			if (typeof(p.populate) === "string")
				p.populate = p.populate.replace(/,/g, " ").split(" ");

			if (typeof(p.searchFields) === "string")
        p.searchFields = p.searchFields.replace(/,/g, " ").split(" ");

        if (!p.include) {
          p.include = []
        }


			if (ctx.action.name.endsWith(".list")) {
				// Default `pageSize`
				if (!p.pageSize)
					p.pageSize = this.settings.pageSize;

				// Default `page`
				if (!p.page)
					p.page = 1;

				// Limit the `pageSize`
				if (this.settings.maxPageSize > 0 && p.pageSize > this.settings.maxPageSize)
          p.pageSize = this.settings.maxPageSize;


				// Calculate the limit & offset from page & pageSize
				p.limit = p.pageSize;
				p.offset = (p.page - 1) * p.pageSize;
			}
			// Limit the `limit`
			if (this.settings.maxLimit > 0 && p.limit > this.settings.maxLimit)
				p.limit = this.settings.maxLimit;

			return p;
		},
    populateDocs(ctx, docs, populateFields) {
      if (!this.settings.populates || !Array.isArray(populateFields) || populateFields.length === 0) return Promise.resolve(docs);

      if (docs == null || (!_.isObject(docs) && !Array.isArray(docs))) return Promise.resolve(docs);

      const promises = [];
      _.forIn(this.settings.populates, (rule, field) => {
        if (populateFields.indexOf(field) === -1) return; // skip

        // if the rule is a function, save as a custom handler
        if (_.isFunction(rule)) {
          rule = {
            handler: Promise.method(rule),
          };
        }

        // If string, convert to object
        if (_.isString(rule)) {
          rule = {
            action: rule,
          };
        }
        rule.field = rule.field || field;
        const arr = Array.isArray(docs) ? docs : [docs];

        // Collect IDs from field of docs (flatten, compact & unique list)
        const idList = _.uniq(_.flattenDeep(_.compact(arr.map(doc => doc[rule.field]))));
        // Replace the received models according to IDs in the original docs
        const resultTransform = (populatedDocs) => {
          arr.forEach((doc) => {
            const id = doc[rule.field];
            if (_.isArray(id)) {
              const models = _.compact(id.map(id => populatedDocs[id]));
              doc[field] = models;
            } else {
              doc[field] = populatedDocs[id];
            }
          });
        };

        if (rule.handler) {
          promises.push(rule.handler.call(this, idList, arr, rule, ctx).then(resultTransform));
        } else if (idList.length > 0) {
          // Call the target action & collect the promises
          const params = Object.assign(
            {
              id: idList,
              mapping: true,
              populate: rule.populate,
            },
            rule.params || {},
          );

          promises.push(ctx.call(rule.action, params).then(resultTransform));
        }
      });

      return Promise.all(promises).then(() => docs);
    },
  },
};
