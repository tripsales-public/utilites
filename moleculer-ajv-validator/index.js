const Ajv = require('ajv');

const BaseValidator = require('moleculer/src/validator');

class AjvValidator extends BaseValidator {
  constructor(opts) {
    super();
    this.validator = new Ajv(opts);
  }

  compile(schema) {
    const validate = this.validator.compile(schema);
    return (params) => this.validate(params, validate);
  }

  validate(params, validate) {
    const isValid = validate(params);
    
    if (isValid) return true;

    return validate.errors;
  }
}

module.exports = AjvValidator;
